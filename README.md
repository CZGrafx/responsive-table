# README #

Simple reponsive table layout. Normal Table on desktop and tablet, stacked table on mobile.

### What is this repository for? ###

* Table I used for a site, started to use bootstrap but didn't like the way it flowed so I can up with this option. Uses JQuery.
* v1.0

### How do I get set up? ###

* To use on you site or project, just download repository. Open index.html and copy out the table format.
* Replace content or remove unwanted field. If you want to add more simply copy the direct one above and paste bellow, then replace content.
* Move style.css file into your site or project and link inside the head tag.

### Who do I talk to? ###

* Repo owner or admin